#!/bin/bash

until nc -z -v -w30 db 3306
do
  echo "Waiting for database..."
  sleep 5
done

php artisan migrate --force
php artisan db:seed

exec "$@"
