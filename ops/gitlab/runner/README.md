# Gitlab runner

## Run (No root)

- Build image

```
docker build --no-cache -t lostops-challenge-gitlab-runner:1.0.0 .
```

- Run

```
 docker run -it -d --name lostops-challenge-gitlab-runner --restart always -e URL=<URL> -e TOKEN=<TOKEN> -e RUNNER_ALLOW_RUNASROOT="1" -v /var/run/docker.sock:/var/run/docker.sock lostops-challenge-gitlab-runner:1.0.0
```