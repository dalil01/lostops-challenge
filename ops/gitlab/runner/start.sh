#!/bin/sh

# To generate config.toml -> gitlab-runner register --url https://gitlab.com  --token $TOKEN
# The result of register command
echo "
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  url = '$URL'
  token = '$TOKEN'
  executor = 'docker'
  [runners.docker]
    tls_verify = false
    image = 'docker:stable'
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ['/cache']
    shm_size = 0
    network_mtu = 0
  [runners.cache]
    MaxUploadedArchiveSize = 0
" > "/etc/gitlab-runner/config.toml"

gitlab-runner run